<?php
require 'partials/header.php';
require 'partials/nav.php';
?>

<table>
    <thead>
        <tr>
            <th>Product</th>
            <th>Prijs</th>
            <th>Aantal</th>
            <th>Subtotaal</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($groceryList->getItems() as $i => $item) : ?>
            <tr class="product-<?= $i ?>">
                <td><?= $item->name ?></td>
                <td class="productPrice"><?= $item->price ?></td>
                <td>
                    <button>-</button>
                    <input class="productQuantity" type="number" min="0" value="<?= $item->number ?>">
                    <button>+</button>
                </td>
                <td class="productTotalCost"><?= $item->getSubTotal() ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="3">Totaal</td>
            <td id="totalCost"></td>
        </tr>
    </tfoot>
</table>

<?php
require 'partials/footer.php';
