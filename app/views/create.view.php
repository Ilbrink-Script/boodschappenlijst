<?php
require 'partials/header.php';
require 'partials/nav.php';

if (isset($formError)): ?>
<p class="error"><?=$formError?></p>
<?php endif; ?>

<form id="create" method="post">
    <label for="name">Naam</label>
    <input type="text" name="name" placeholder="Product naam" value="<?=$name?>">

    <label for="number">Aantal</label>
    <input type="number" name="number" placeholder="1" min="1" value="<?=$number?>">

    <label for="price">Prijs</label>
    <input type="number" name="price" placeholder="1.00" step="0.01" value="<?=$price?>">

    <button>Versturen</button>
</form>

<?php
require 'partials/footer.php';
