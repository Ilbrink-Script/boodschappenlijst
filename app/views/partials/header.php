<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Opdracht 18 - PHP Boodschappenlijst</title>
    <link rel="icon" type="image/png" sizes="16x16" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAM8SURBVDhPdZNdbBRVGIbfM7O/3Zkddpay07XdUoooNLYRQVIToCpgIGJIbBsv5KLqDZiQmIjxwgtICBe98NZwx43BAAlZNLFIi2ultBcltLZuAdtttn9bum3t7sxsZ8/uznFmu4Eg4U1Ozk++5/2+7+Qcgv+p948vd+e07Le6qrY9jK8JmqbPxO7oqyCmLvvc527fHR2ohJb1nMHZs6c6mGn+mFVXnUAAVZ5VOHkR16O9yOl5+Ko8rDrgvxKuDnx949afKZvhy6Slq5cOSWsF3AmHDlQdP/EVprVmGM7X8WqDhJHRCei6BkEUSF19w841PXclmZxbtDmuTFvyKdL+nc0RqWVfE6oVgua6ceTmb+LWb30IKSHIsgy5WqE87/y0v3/wQQV71sLprtb28I7wNcG/A/9MEUyMjSAx+RCmaWLdMPBKJEI3BeVjsZ6evgpS1tMWXmtQuLnlwmkTWUxNLiA+9ne57AI1rEGxmk7zqdmZw/ve2jU4M7c4W8HgqMxo6YqcqXVGUF/bhqWlRQzEKIZ+Hwch1DLikaebwHNcjeD3nrLC721QlRa6o+0dhLCrPrcTbioiJHgQjaXgzxC0HnyMqbEtSOlFzNHtMBreeFyUQkM9iXgXzp83ue5fOhQL/sE2ytM8GqW/MH7/V4ilMXC+POaTASRXHmFleZq5pPVMUVJUK2/TB1t3HbUZ0h39+AYhOGFvbL1bM4/NwfehqQ/g8rZAzxdNApUCJThcW/DzwjvzokfhZtXs5NAAjnFWdq3ClmUwAaL/bSjhL6BqI6gJHSJK6LgjKB/kQ8H3HPvr6wL1Ack7sbQS9DY+Ocp/eHJ3X9EsfEJAAraB7DIgC4pVwTC8vjctk7hJC0ulvLHARtMk8/2wlnQ7OG7636wBhtqNS7zZ3koY67eu1LFNXMM2MWMfY930wWRckYBRBsIuJToTaRq02tmQlTRafonffHR9kDF2wV5nqBslhkUXV0w5SWHZhu3zuLo98zxs9gpedvHpU95K+QuMmWdW1qua9uwdrhENT+NmT+aAk9DPJrVIfyy9d7kSCkbY7Vxi4rtrnZ2lF77zS0SOXP6pjSf854D5SPCSizYMAP8BLelYmrw4yYgAAAAASUVORK5CYII=">
    <link rel="stylesheet" href="/style.css">
    <script src="/script.js"></script>
</head>

<body>
<main>