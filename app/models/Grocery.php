<?php

namespace App\Models;

class Grocery {
    public $name, $number, $price;

    function getSubTotal() {
        return number_format($this->number * $this->price, 2);
    }
}