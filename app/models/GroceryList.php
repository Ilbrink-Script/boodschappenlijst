<?php

namespace App\Models;

class GroceryList
{
    private $items = [];

    function __construct($groceries = [])
    {
        $this->items = $groceries;
    }

    function addItem(Grocery $item)
    {
        return array_push($this->items, $item);
    }

    function getItems()
    {
        return $this->items;
    }

    // calculates the total for all items
    function getTotal()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->getSubTotal();
        }
        return $total;
    }
}
