<?php

$router->get('groceries', 'GroceriesController@index');
$router->get('groceries/create', 'GroceriesController@create');
$router->post('groceries/create', 'GroceriesController@create');
