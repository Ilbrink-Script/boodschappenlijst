<?php

namespace App\Controllers;

use App\Core\{App, Request};
use App\Models\{GroceryList};

class GroceriesController
{
    public function index()
    {
        $groceries = App::get('database')->selectAll('groceries', 'Grocery');
        $groceryList = new GroceryList($groceries);
        return view('index', compact('groceryList'));
    }

    public function create()
    {
        // params will be NULL when not posted
        $data =  [
            'name' => filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING),
            'number' => filter_input(INPUT_POST, 'number', FILTER_SANITIZE_NUMBER_INT),
            'price' => filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)
        ];

        if (Request::requestType() === 'POST') {

            if (App::get('database')->insert('groceries', $data)) {
                header('Location: /groceries');
            } else {
                $data['formError'] = 'Er ging iets niet goed, probeer het nog een keer.';
            }
        }

        return view('create', $data);
    }
}
