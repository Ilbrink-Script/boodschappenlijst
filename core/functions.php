<?php

function dd($data) {
    // echo '<pre>';
    var_dump($data);
    // echo '</pre>';
    exit();
}

// breaks if there is an key with the same name as the first parameter ($name for example)
function view($viewName, $data = []) {
    extract($data);
    return require "app/views/{$viewName}.view.php";
}