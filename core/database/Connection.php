<?php

namespace App\Core\Database;
use PDO;

class Connection {

    public static function make($config) {
        try {
            $dataSourceName = $config['connection'] . ';dbname=' . $config['name'];
            return new PDO($dataSourceName, $config['username'], $config['password']);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

}