-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 29 aug 2020 om 12:56
-- Serverversie: 10.4.13-MariaDB
-- PHP-versie: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilbrink_script_boodschappenlijst`
--
DROP DATABASE IF EXISTS `ilbrink_script_boodschappenlijst`;
CREATE DATABASE IF NOT EXISTS `ilbrink_script_boodschappenlijst` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ilbrink_script_boodschappenlijst`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `groceries`
--

DROP TABLE IF EXISTS `groceries`;
CREATE TABLE `groceries` (
  `name` text NOT NULL,
  `number` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `groceries`
--

INSERT INTO `groceries` (`name`, `number`, `price`) VALUES
('Boter', 3, '2.20'),
('Kaas', 1, '8.45'),
('Eieren', 2, '1.29'),
('Bier', 6, '12.00'),
('Dropjes', 2, '2.49'),
('Tijgerbrood', 1, '1.89'),
('Spinazie', 1, '2.89'),
('Karnemelk', 1, '0.77'),
('Tandpasta', 3, '3.49');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


GRANT USAGE ON *.* TO `ilbrink_script`@`localhost` IDENTIFIED BY PASSWORD '*E1BAA9E55F49FAFD156B2B3F3526FF99B62518A0';

GRANT ALL PRIVILEGES ON `ilbrink\_script\_%`.* TO `ilbrink_script`@`localhost`;
GRANT ALL PRIVILEGES ON `ilbrink\_script\_%`.* TO 'Paul Ilbrink'@'localhost';
