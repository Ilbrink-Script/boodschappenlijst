<?php

namespace App\Core\Database;
use PDO;

class QueryBuilder
{

    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function selectAll($table, $intoClass = '', $namespace = 'App\\Models\\')
    {
        $stmt = $this->pdo->prepare("SELECT * FROM {$table}");
        $stmt->execute();

        if ($intoClass) {
            return $stmt->fetchAll(PDO::FETCH_CLASS, $namespace . $intoClass);
        }

        return $stmt->fetchAll();
    }

    // inserts parameters in table, returns true if successful
    public function insert($table, $parameters)
    {
        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $table,
            implode(", ", array_keys($parameters)),
            implode(", ", array_map(function ($val) {
                return ':' . $val;
            }, array_keys($parameters)))
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            return $stmt->execute($parameters);
        } catch (Exception $e) {
            //
            die('Insert failed.');
        }
    }
}
