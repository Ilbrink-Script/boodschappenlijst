<?php

namespace App\Core;

class Router
{
    protected $routes = [
        'GET' => [],
        'POST' => [],
        'PUT' => [],
        'PATCH' => [],
        'DELETE' => []
    ];

    public static function load($file)
    {
        $router = new static;
        require $file;
        return $router;
    }

    private function addRoute($requestType, $uri, $controller)
    {
        return $this->routes[$requestType][$uri] = $controller;
    }

    public function get($uri, $controller)
    {
        return $this->addRoute('GET', $uri, $controller);
    }
    public function post($uri, $controller)
    {
        return $this->addRoute('POST', $uri, $controller);
    }

    public function put($uri, $controller)
    {
        return $this->addRoute('PUT', $uri, $controller);
    }

    public function patch($uri, $controller)
    {
        return $this->addRoute('PATCH', $uri, $controller);
    }

    public function delete($uri, $controller)
    {
        return $this->addRoute('DELETE', $uri, $controller);
    }

    public function direct($uri, $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            // die(var_dump(explode('@', $this->routes[$requestType][$uri])));
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri])
            );
        }
        throw new Exception("No route defined for this URI [${requestType}::$uri].");
    }

    protected function callAction($controller, $action)
    {
        $controller = "App\\Controllers\\{$controller}";

        if (method_exists($controller, $action)) {
            return (new $controller)->$action();
        }

        throw new Exception("{$controller} doesn't not respond to the {$action} action.");
    }
}
